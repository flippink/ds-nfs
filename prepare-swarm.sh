#!/bin/bash

mkdir /nfs

mount -t nfs -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport ${flippink_nfs_url}:/  /nfs

cd /nfs

chmod go+rw .

mkdir volumes

mkdir cronjobs

mv /ds-nfs/nfs-permissions-m.sh /nfs/cronjobs/nfs-permissions-m.sh

mv /ds-nfs/nfs-permissions-h.sh /nfs/cronjobs/nfs-permissions-h.sh

mv /ds-nfs/startup.sh /startup.sh