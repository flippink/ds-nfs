#!/bin/bash

# mounting nfs #

mkdir /nfs

mount -t nfs -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport ${flippink_nfs_url}:/  /nfs

# script permissions #

chmod 700 /nfs/cronjobs/nfs-permissions-h.sh

chmod 700 /nfs/cronjobs/nfs-permissions-m.sh

./nfs/cronjobs/nfs-permissions-m.sh

./nfs/cronjobs/nfs-permissions-m.sh

# setting permissions for ssl cert file and restarting traefik service #

chmod 600 /nfs/traefik/acme.json

docker service update loadbalancer_traefik