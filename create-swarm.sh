#!/bin/bash

mkdir /nfs/volumes/portainer_data

mkdir /nfs/volumes/prometheus_data

mkdir /nfs/volumes/grafana_data

mkdir /nfs/volumes/redis_data

mkdir /nfs/volumes/blackbox_data

mkdir /nfs/loadbalancer && mkdir /nfs/dashboard

mv /ds-nfs/loadbalancer/docker-compose.yml /nfs/loadbalancer/docker-compose.yml

mv /ds-nfs/dashboard/docker-compose.yml /nfs/dashboard/docker-compose.yml

mkdir /nfs/monitoring

mv /ds-nfs/monitoring/grafana.ini /nfs/monitoring/grafana.ini

mv /ds-nfs/monitoring/prometheus.yml /nfs/monitoring/prometheus.yml

mv /ds-nfs/monitoring/docker-compose.yml /nfs/monitoring/docker-compose.yml

rm -rf /ds-nfs/loadbalancer && rm -rf /ds-nfs/dashboard && rm -rf /ds-nfs/monitoring

# ssl cert file and permissions for traefik #

mkdir /nfs/traefik

touch /nfs/traefik/acme.json

chmod 600 /nfs/traefik/acme.json

docker swarm init

# create docker networks for main stacks #

docker network create --driver overlay --attachable traefik || true

docker network create --driver overlay --attachable loadbalancer || true

docker network create --driver overlay --attachable dashboard || true

docker network create --driver overlay --attachable monitoring || true

# deploy main stacks #

docker stack deploy --compose-file /nfs/loadbalancer/docker-compose.yml loadbalancer

echo sleeping for 1m

sleep 1m

docker stack deploy --compose-file /nfs/dashboard/docker-compose.yml dashboard

echo sleeping for 1m

sleep 1m

docker stack deploy --compose-file /nfs/monitoring/docker-compose.yml monitoring